#Adding Additional dunctionallity in the function this work is done by the Decorator

'''
3 things are compulsory in Decorator function

1. always function has function parameter
2. function has Compulsory at least one Inner function
3. decorator function must have to return Inner function address
'''

def decorfun(func):

    def wrapper():
        print("Start Wrapper")
        func();
        print("End Wrapper")
        return 10;
    return wrapper;


def normalfun():
    print("In NOrmal fun")
    return 20;

normalfun=decorfun(normalfun)
print(normalfun())#Wrapper()

'''
output
start Wrapper
In Normal fun
end Wrapper
10'''
