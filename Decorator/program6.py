#Decorator Chaining

def decorfun(func):
    print("In decorfun");

    def wrapper():
        print("start wrapper1")
        func();#wrapper2
        print("End Wrapper1")

    return wrapper;

def decorrun(func):
    print("In decorrun")

    def wrapper():
        print("Start wrapper2")
        func();#normalfun
        print("End Wrapper2")

    return wrapper;

def normalfun():
    print("In normal fun")

normalfun=decorfun(decorrun(normalfun));
normalfun()

'''output
In decorrun
In decorfun
start wrapper1
start wrapper2
in normal fun
end wrapper2
end wrapper1
'''



