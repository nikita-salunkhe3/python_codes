def decorfun(func):

    print("In decorfun");

    def wrapper(*args):
        print("Start wrapper")
      # print(func(*args))
        print("end wrapper")
        return 100;
    return wrapper


def normalfun(x,y):
    print("In normal fun")
    return x+y

normalfun = decorfun(normalfun(10,20));
print(normalfun(10,20))

