#Keyword arguments to function (**nikita)

def fun(x,y,z=30,*args):
    print(x)
    print(y)
    print(z)
    print(args)  #() => internally tuple ahe

fun(10,20,400)


def gun(x,y,**argv):
        print(x)
        print(y)
        print(argv)   #{'z'=10}  => internally dictionary
        print(type(argv))  #<class 'dict'>

gun(x=1,y=2,z=10)
