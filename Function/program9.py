#Passing Number of arguments to function

def fun(*argv):      #*argv==>>> Variable Argument
    print(argv)
    print(type(argv))

fun()
fun(1)
fun(1,2,3)
