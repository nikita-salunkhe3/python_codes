'''
1  2  3  4  5
6  7  8  9  10
12 18 20 21 24
27 30 36 40 42
45 48 50 54 60

'''

def fun(n):
    sum=0
    while(n != 0):
        sum=sum+(n%10)
        n=n//10
    return sum

row=int(input("Enter the row"))

num=1
for i in range(row):
    for j in range(row):
        flag=0;
        while(flag != 1):
            sum=fun(num)
            if(num % sum ==0):
                flag=1;
                print(num,end=" ")
                num=num+1
            else:
                num=num+1
    print();





        
