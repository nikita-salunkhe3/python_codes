#type 4

x=10
y=20.5
print(x)
print(y)


print("Value of x:%d"%x)  #Value of 10
print("Value of x:%i"%x)  #Value of 10
print("Value of y:%f"%y)  #Value of 20.500000

print("Value of x={} and Value of y={}".format(x,y))
