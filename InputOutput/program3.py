#type-2
#print(String)

print('core2web')       #core2web
print('''core2web''')   #core2web
print("core2web")       #core2web
print("""core2web""")   #core2web

print('core2web'+"Incubator")  #core2webIncubator
print('core2web','Incubator')  #core2web Incubator  
print(3%'Biencaps')            #BiencapsBiencapsBiencaps
