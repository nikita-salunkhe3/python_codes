#3. SEQUENCE TYPE
#1.String
'''
String python madhe bydefault 4 type madhe lihita yete
bydefault python string la ('   ') single coat madhe support kerte 
python char ya datatype la consider karat nahi
jeri apan single character menun dil teri to tya character la string ch consider kerto

'''

friend1='kanha';
print(friend1);        #kanha
print(type(friend1));  #<class 'str'>

friend2="Rahul";
print(friend2);       #Rahul
print(type(friend2)); #<class 'str'>

friend3='''Ashish''';
print(friend3);      #Ashish
print(type(friend3));#<class 'str'>

friend4="""Badhe""";
print(friend4);       #Badhe
print(type(friend4)); #<class 'str'>

char='A';
print(char)         #A
print(type(char))   #<class 'str'>

#2. List
'''
-It is like a container that can store various types of data
-List can accecpt duplicate data also.
-LIST is MMUTABLE type , because it can change their that.
'''

empList=[18,'ashish',25.5,11,"Kanha",35.5]
print(empList);       #[18,'ashish',25.5,11,'Kanha',35.5]
print(type(empList))   #<class 'list'>

list1=[11,12,12,12,12];
print(list1);            #[11,12,12,12,12]
print(type(list1));     #<class 'list'>

#3.Tuple

empTuple=(18,"Ashish",25.5,11,"Kanha",35.5)
print(empTuple);        # empTuple list print hoil
print(type(empTuple))   #<class 'tuple'>

'''
empTuple[4]="Rahul"; #error
print(empTuple);     #<class 'tuple'>
'''





#3.range

x=range(10);
print(x);         #range(0,10)
print(type(x))     #<class 'range'>

x=range(21,25)
print(x)
print(type(x))
