#1. NUMERIC TYPE
#1. int

empId=10;
print(empId);    #10
print(type(empId));#<class 'int'>

jerNo=7;
print(jerNo);       #7
print(type(jerNo));  #<class 'int'>



#2. float
imdb=9.5;
print(imdb);            #9.5
print(type(imdb));      #<class 'float'>

data=45.777777778888888999999999999999;
print(data);           #45.777777777888889
print(type(data));     #<class 'float'>




#3.Complex Number
complexData=10+5j
print(complexData)          #  (10+5j)
print(type(complexData));    #<class 'complex'>

data=23.43+23.1j;
print(data)            #(23.43+23.1j)
print(type(data));     #<class 'complex'>







