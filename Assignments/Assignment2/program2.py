'''
2. WAP to determine whether entered angles define a right-angled triangle.
Take three values of angle from the user.
Input1: angel1 = 90
Input2: angle2 = 90
Input3: angle3 = 90
Output:
It is not a right-angle triangle
Input1: angel1 = 90
Input2: angle2 = 60
Input3: angle3 = 30
Output:
It is a right-angle triangle
'''

angel1=int(input("Enter the angle"))
angel2=int(input("Enter the angle"))
angel3=int(input("Enter the angle"))

if((angel1 + angel2 + angel3) == 180):
    print("It is a right-angle Triangle")
else:
    print("It is Not a right_angle Triangle")
