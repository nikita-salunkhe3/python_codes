'''
. Print the "Core2web" string a number of times entered by the user if the
number is even.
#Input: num = 2
#Output: Core2web
Core2web
#Input: num = 5
#Output: No Output
'''

str1=input("Enter the Sting")
num=int(input("Enter the number"))
if(num % 2==0):
    print(num*str1)
