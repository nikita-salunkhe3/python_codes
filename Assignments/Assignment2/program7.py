'''
7. Take two numbers from the user, check if both are odd and then print the
sum of the numbers.
#Input: num1 = 10
#Input: num2 = 11
#Output: 21
#Input: num1 = 10
#Input: num2 = 6
#Output: No Output
'''

num1=int(input("Enter the number"));
num2=int(input("Enter the number"));

if(num1 % 2 == 1 or num2 % 2 == 1):
    print(num1+num2)
