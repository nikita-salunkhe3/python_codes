'''
8. Program 8: Write a program to check whether the number is greater than 10 or
not
Input: 12
Output: yes 12 is greater than 10
Input: 2
Output: no 2 is less than 10
'''

num=int(input("Enter the number"))

if(num > 10):
    print(num,"is greater than 10")
elif(num < 10):
    print(num,"is less than 10")
else:
    print(num,"is equal to 10")
