'''
2. Program 2: Write a Program to check whether the number is Negative,
Positive or equal to Zero.
Input: -2
Output: -2 is the negative number
'''

num1=int(input("Enter the number : "))

if(num1 > 0):
    print(num1,'is greater than 0')
elif(num1 < 0):
    print(num1,'is less than 0')
else:
    print("number is Zero")

