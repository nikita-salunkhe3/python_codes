'''
1. Program 1: Write a Program to Find a Maximum between two numbers
Input: 1 2
Output: 2 is Max number between 1 & 2
'''

num1=int(input("Enter the 1st Number : "))

num2=int(input("Enter the second Number : "))

if(num1 > num2):
    print(num1,"is greater than",num2)
elif(num2 > num1):
    print(num2,"is greater than",num1)
else:
    print("Both are same")

