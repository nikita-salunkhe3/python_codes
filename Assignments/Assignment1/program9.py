'''
9. Program 9: Write a program to check whether the input character is a vowel or
consonant
Input: ‘a’
Output: vowel
Input: ‘b’
Output: consonant
'''

ch=input("Enter the character")

if(ch == 'A'or ch == 'a' or ch == 'E' or ch == 'e' or ch == 'o' or ch == 'O' or ch == "U" or ch== 'u'or ch== 'i' or ch == 'I'):
    print(ch,'is vowel')
else:
    print(ch,'is consonant')

