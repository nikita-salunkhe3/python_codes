'''
7. Program 7: Write a Program to take a number of months and print the number of
days in that month.
Input: 4
Output: April is a 30-day month
'''

month = int(input("Enter the month number"))

if(month == 1):
    print("January is a 31-day month")
elif(month == 2):
    year=int(input("Enter the year"))
    if(year % 4 == 0):
        print("feb is a 28-day month")
    else:
        print('feb is a 27-day month')
elif(month == 3):
    print('March is a 31-day month')
elif(month == 4):
    print('April is a 30-day month')
elif(month == 5):
    print('May is a 31-day month')
elif(month == 6):
    print('June is a 30-day month')
elif(month == 7):
    print('July is a 31-day month')
elif(month == 8):
    print('August is a 31-day month')
elif(month == 9):
    print('September is a 30-day month')
elif(month == 10):
    print('Octomber is a 31-day month')
elif(month == 11):
    print('November is a 30-day month')
elif(month == 12):
    print('December is a 31-day month')
else:
    print('Invalid month number')
