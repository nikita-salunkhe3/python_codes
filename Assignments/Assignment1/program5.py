'''
Program 5: Write a Program to take an integer ranging from 0 to 6 and print
corresponding weekday (week starting from Monday)
Input: 2
Output: Wednesday
'''

day = int(input("Enter the number"))

if(day == 1):
    print('Monday')
elif(day == 2):
    print("Tuesday")
elif(day == 3):
    print('wednesday')
elif(day == 4):
    print('Thursday')
elif(day == 5):
    print('Friday')
elif(day == 6):
    print('Saturday')
elif(day == 7):
    print("sunday")
else:
    print("Invalid day count")
