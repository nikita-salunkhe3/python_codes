'''
3. Program 3: Write a Program to Find whether the number Is Even or Odd
Input: 4
Output: 4 is an Even Number!
'''

num=int(input("Enter the number: "))

if(num % 2 == 0):
    print(num,"is even")
else:
    print(num,"is odd")
