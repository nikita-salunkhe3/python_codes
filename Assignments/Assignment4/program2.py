'''
Write a Program to print all Even numbers from a given range.
Input :
Start = 10;
End = 20;
Output:
10 12 14 16 18
'''

start=int(input("Enter the starting number"))
end=int(input("Enter the ending number"))

for i in range(start,end):
    if(i % 2 == 0):
        print(i,end="  ")
print()
