'''
WAP to print all the character values of the given ASCII value range from the
user
Input :
Enter the start of range : 1
Enter end of range: -2
Output :
Wrong input
Input :
Enter start of range : 65
Enter end of range : 67
Output :
The character of ASCII value 65 is A.
The character of ASCII value 66 is B.
The character of ASCII value 67 is C.
.
.
.
The character of ASCII value 89 is Y.
'''

# A To Z ==>> 65 to 90
# a to z ===>>97 to 120
start = int(input("Enter the starting number"))
end = int(input("Enter the ending number"))

if(start < end):
    if(((start >= 65 and start <= 90) or (start>= 97 and start<=120)) and (end >=65 and end <=90)or(end>=97 and end<=120)):
        for i in range(start,end+1):
            print("The character of ASCII value",i,")
else:
    print("Wrong input")
        

