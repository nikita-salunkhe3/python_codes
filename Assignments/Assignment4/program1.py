'''
WAP to print numbers from a given range.
Input :
Start = 100;
End = 110;
Output:
100 101 103 104 105 106 107 108 109
'''

for i in range(100,110):
    print(i,end=" ")

print()
