# Input/Output

x=10
y=20
print(x+y)  #30

p=input("Enter value for p: "); #1
q=input("Enter value for q: "); #2

print(p+q);    #12

print(type(p)) #<class 'str'>
print(type(q)) #<class 'str'>

a=int(input("Enter the value for a: "))   #100
b=int(input("Enter the value for b: "))   #200

print(a+b); #300

print(type(a))  #<class 'int'>
print(type(b))  #<class 'int'>
