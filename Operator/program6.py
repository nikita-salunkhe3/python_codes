#Identity Operator(is, is not)
#Membership operator(in, not in)

tuplelist=('Nikita',11,'Sarthak','10','Shweta','13')
x=11
a=11
y=10

print(id(x))   #0x100
print(id(a))   #0x100
print(id(y))   #0x200

print(x is y)        # False
print(x is not y)    #True

print(x in tuplelist) #True
print(x not in tuplelist) #False
