class Company:

    name="Veritas"

    def __new__(self):
        print("In new")
        return super().__new__(self)
   
    def __init__(self):
        print("In init")
        self.empcount=30631
        self.valuation=643.52

    def fun(self):         #In fun it is the non-static method and in non-static method non-static
        #vairable are access also class variables are also access

        print("In Instance method")
        print(self.empcount)
        print(self.valuation)
        print(self.name)
        print(Company.name)

    @classmethod
    def gun(self):
        print("In class Method")
        print(self)
    #   print(self.empcount)  
   #    print(self.valuation)
        print(self.name)
        print(Company.name)

    @staticmethod
    def run():
        print("In static Method")
        print(Company.name)



Company()

obj=Company()

obj.fun()
#Company.fun()  --------TypeError

print(obj)
obj.gun();
Company.gun();

Company.run()
obj.run()





