class Employee:

    def __new__(cls):
        print(cls)
        print("Object Creator on Heap section")
        return object.__new__(cls)

    def __init__(self):
        print("In Constructor")
        print(self)

obj=Employee();


'''
flow of the code
1. __new__(self) la call jato ani yane heap section ver object create karun deil
2. __new__ ya method madhun parent cha __new__ method la call jato
3. nanter Parent cha __new__ method madhun __init__ la call aplya class la jato
'''
