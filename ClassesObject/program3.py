class Company:

    def __init__(self):
        print("In constructor")
        name="facebook";

    @staticmethod
    def compInfo(self):
        print("In static method")

    @classmethod
    def comp(self):
        print("In class Method")

obj=Company();

obj.compInfo();
Company.compInfo();

obj.comp();
Company.comp();
