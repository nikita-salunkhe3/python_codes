class Player:
    match="Cricket"

    def __new__(self,name,jerNo):
        print("In new");
        return object.__new__(self)

    def __init__(self,name,jerNo):
        print("In init")
        self.name=name
        self.jerNo=jerNo

    def fun(self):
        print("In Non_static method")
        print(self)

    @classmethod
    def gun(cls):
        print("In Class Method")
        print(cls)

obj=Player("Virat",18)
print("In main obj is : ",obj)
obj.fun()
obj.gun()
